import 'package:decimal/decimal.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:physics_calculator/ui/widgets/fields.dart';
import 'package:physics_calculator/units.dart';

class Calculation {
  String name;
  List<Param> params;
  List<Formula> formulas;

  Calculation(this.name, this.params, this.formulas);

  Future<Decimal> calculate(Formula formula, List<ValuedParam> params) {
    ContextModel model = ContextModel();
    params.forEach((element) =>
        model.bindVariableName(element.parent.id, Number(element.value)));
    var res =
        formula.expression.evaluate(EvaluationType.REAL, model).toString();
    if (res == "NaN") {
      return Future.error(res);
    }
    return Future.value(Decimal.parse(res));
  }

  Formula fromParam(Param param) {
    return formulas.firstWhere((element) => element.self == param);
  }
}

class Param {
  String id;
  String name;
  Units units;
  String latex;
  Field field;

  Param(this.id, this.name, this.units, this.latex, this.field);
}

class ValuedParam {
  Param parent;
  double value;

  ValuedParam(this.parent, this.value);
}

class Formula {
  Param self;
  Expression expression;
  String latex;
  List<Param> params;

  Formula(this.self, this.expression, this.latex, this.params);

  String get unitsFormula {
    String right = latex.split("=")[1];
    params.forEach((element) {
      var startSlash = element.latex.startsWith('\\');
      right = right
          .replaceAll(
              RegExp(
                  r"(?<!(\w(?!\\)))(((\\)|(?<!\w))(cos|sin|acos|asin)([^ \\()]+)?)"),
              "")
          .replaceAll(
              RegExp(
                  "(?<!(\\w(?!\\\\)))(${startSlash ? '\\' + element.latex : element.latex})(?!\\w)"),
              element.units.showUnit
                  ? startSlash
                      ? ' ' + element.units.defaultUnit.value
                      : element.units.defaultUnit.value
                  : '-');
    });
    return self.units.defaultUnit.value + '=' + right;
  }

  @override
  bool operator ==(Object other) => other is Formula && other.latex == latex;

  @override
  int get hashCode {
    int hash = 23;
    hash = hash * 31 + latex.hashCode;
    return hash;
  }

  @override
  String toString() {
    return latex;
  }
}
