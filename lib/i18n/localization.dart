import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

import 'strings.dart';

class Translator {
  final Locale locale;

  Translator(this.locale);

  static Translator of(BuildContext context) {
    return Localizations.of(context, Translator);
  }

  String translate(Strings key, {List<dynamic>? params}) {
    String value = localizedSimpleValues[locale.languageCode]?[key] ?? key.name;
    if (params != null && params.isNotEmpty) {
      for (int i = 0, length = params.length; i < length; i++) {
        value = value.replaceAll('%\$$i\$s', '${params[i]}');
      }
    }
    return value;
  }

  String translateFromString(String key, {List<dynamic>? params}) {
    Strings? strings = Strings.fromName(key);
    if (strings == null) {
      return key;
    }
    String value = localizedSimpleValues[locale.languageCode]?[strings] ?? strings.name;
    if (params != null && params.isNotEmpty) {
      for (int i = 0, length = params.length; i < length; i++) {
        value = value.replaceAll('%\$$i\$s', '${params[i]}');
      }
    }
    return value;
  }

  static String get(BuildContext context, Strings key, {List<dynamic>? params}) {
    return of(context).translate(key, params: params);
  }

  static String getFromString(BuildContext context, String key, {List<dynamic>? params}) {
    return of(context).translateFromString(key, params: params);
  }
}

class TranslationDelegate extends LocalizationsDelegate<Translator> {
  const TranslationDelegate();

  @override
  bool isSupported(Locale locale) => supportedLanguages.contains(locale);

  @override
  Future<Translator> load(Locale locale) =>
      SynchronousFuture<Translator>(new Translator(locale));

  @override
  bool shouldReload(covariant LocalizationsDelegate<Translator> old) => true;
}

class SpecifiedTranslationDelegate extends TranslationDelegate {
  final Locale overriden;

  const SpecifiedTranslationDelegate(this.overriden);

  @override
  bool isSupported(Locale locale) {
    return super.isSupported(locale);
  }

  @override
  Future<Translator> load(Locale locale) =>
      SynchronousFuture<Translator>(Translator(overriden));
}

List<Locale> get supportedLanguages {
  return localizedSimpleValues.keys.map((e) => Locale(e)).toList();
}
