import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart'
    show rootBundle;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:path_provider/path_provider.dart';
import 'package:physics_calculator/calculation.dart';
import 'package:physics_calculator/parsers.dart';
import 'package:physics_calculator/theme/dark.dart';
import 'package:physics_calculator/ui/app.dart';
import 'package:physics_calculator/ui/create.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'i18n/localization.dart';
import 'i18n/strings.dart';
import 'ui/settings.dart';

void main() => runApp(App());

final themeProvider = DarkThemeProvider();

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AppState();
}

var changeDelegate;

Future<List<Calculation>> loadCalculations() async {
  final directory = await getApplicationDocumentsDirectory();
  File calculations = File("${directory.path}/calculations.xml");
  File choices = File("${directory.path}/choices.xml");

  if (!await choices.exists()) {
    await choices.create();
  }
  await choices
      .writeAsString(await rootBundle.loadString('assets/xml/choices.xml'));

  parseChoices(choices);

  if (!await calculations.exists()) {
    await calculations.create();
  }
  await calculations.writeAsString(
      await rootBundle.loadString('assets/xml/calculations.xml'));
  return Future.value(parseCalculations(calculations));
}

class _AppState extends State<App> {
  var delegate = TranslationDelegate();

  @override
  void initState() {
    changeDelegate = <Void>(TranslationDelegate delegate) {
      setState(() {
        this.delegate = delegate;
      });
    };
    getCurrentAppTheme();

    super.initState();
  }

  void getCurrentAppTheme() async {
    themeProvider.darkTheme =
    await themeProvider.darkThemePreference.getTheme();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: SharedPreferences.getInstance(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          var lg = (snapshot.data as SharedPreferences).getString('lg');
          if (lg != null && lg != 'auto') {
            delegate = SpecifiedTranslationDelegate(Locale(lg));
          }
          return ChangeNotifierProvider(
            create: (_) => themeProvider,
            child:
            Consumer<DarkThemeProvider>(builder: (context, value, child) {
              return _app(_Tabs(), dark: themeProvider.darkTheme);
            }),
          );
        }
        return _app(Scaffold(
          body: SafeArea(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        ));
      },
    );
  }

  _app(Widget home, {int dark = 0}) {
    return MaterialApp(
        title: 'Physics Calculator',
        darkTheme: ThemeData.from(colorScheme: ColorScheme.dark().copyWith(
            primary: Colors.purple[700],
            surfaceTint: Colors.purple[700],
            onPrimary: Colors.white), useMaterial3: true)
            .copyWith(appBarTheme: AppBarTheme(
            backgroundColor: Colors.purple[700],
            foregroundColor: Colors.white)),
        theme: ThemeData.from(colorScheme: ColorScheme.light().copyWith(
            primary: Colors.blue, surfaceTint: Colors.blue, onPrimary: Colors.black), useMaterial3: true)
            .copyWith(appBarTheme: AppBarTheme(
            backgroundColor: Colors.blue, foregroundColor: Colors.white)),
        themeMode: ThemeMode.values[dark],
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [
          delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: supportedLanguages,
        home: home);
  }
}

class _Tabs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TabsState();
}

class _TabsState extends State<_Tabs> {
  final bodies = [HomePage(), CreatePage()];
  var selected = 0;

  PopupMenuItem getButton(Strings text, IconData iconData, var value) =>
      PopupMenuItem(
        child: Row(
          children: [
            Icon(
              iconData,
              size: 20,
              color: Theme
                  .of(context)
                  .colorScheme
                  .onPrimary,
            ),
            Padding(padding: EdgeInsets.only(right: 5)),
            Text(Translator.get(context, text)),
          ],
        ),
        value: value,
      );

  @override
  Widget build(BuildContext context) =>
      Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          title: Text(
            Translator.get(context, Strings.APP_TITLE),
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          actions: [
            PopupMenuButton(
              icon: Icon(
                Icons.more_vert_rounded,
              ),
              itemBuilder: (context) =>
              [
                getButton(Strings.LANGUAGE, Icons.translate, 0),
                getButton(Strings.THEME, Icons.color_lens_rounded, 1),
                getButton(Strings.REPORT_BUG, Icons.bug_report, 2),
                getButton(Strings.ABOUT, Icons.info_outline, 3),
              ],
              onSelected: (value) => openSettings(context, value),
            ),
          ],
        ),
        /*TODO Once page created: bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.calculate), label: "Calculate"),
            BottomNavigationBarItem(icon: Icon(Icons.create), label: "Create")
          ],
          onTap: (value) => setState(() => selected = value),
          currentIndex: selected,
        ),*/
        body: bodies[selected],
      );
}

class NoGlowingScroll extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(BuildContext context, Widget child,
      ScrollableDetails details) {
    return child;
  }
}
