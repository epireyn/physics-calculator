import 'dart:io';

import 'package:decimal/decimal.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:physics_calculator/calculation.dart';
import 'package:physics_calculator/ui/widgets/fields.dart';
import 'package:physics_calculator/units.dart';
import 'package:rational/rational.dart';
import 'package:xml/xml.dart';

final Parser _mathParser = Parser();
Map<String, List<MultipleChoice<Decimal>>> choices = {};

void parseChoices(File file) {
  var document = XmlDocument.parse(file.readAsStringSync());
  document.rootElement.findElements("list").forEach((element) {
    choices[element.getAttribute("id")!] = element
        .findElements("entry")
        .map((e) => MultipleChoice<Decimal>(
            e.getAttribute("name")!, Decimal.parse(e.innerText)))
        .toList();
  });
}

List<Calculation> parseCalculations(File file) {
  return XmlDocument.parse(file.readAsStringSync())
      .rootElement
      .childElements
      .map((p0) => parseCalculation(p0))
      .toList();
}

Calculation parseCalculation(XmlElement element) {
  String name = element.getAttribute("name")!;
  var params = List.of(
      element.getElement("params")!.childElements.map((p0) => _parseParam(p0)));
  var formulas = List.of(element
      .getElement("formulas")!
      .childElements
      .map((p0) => _parseFormula(p0, params)));

  return Calculation(name, params, formulas);
}

Param _parseParam(XmlElement element) {
  Field field;
  String name = element.getAttribute("name")!;
  Units uts = units[element.getAttribute("units")] ??
      Units([Unit(element.getAttribute("units")!, Rational.one)]);

  var fieldChoice = choices[element.getAttribute("choices")];
  if (fieldChoice != null) {
    field = MultipleChoicesField(name, fieldChoice, uts);
  } else {
    field = UnitsField(name, uts);
  }
  return new Param(element.getAttribute("id")!, name,
      uts, element.getAttribute("latex")!, field);
}

Formula _parseFormula(XmlElement element, List<Param> params) {
  var expression = _mathParser.parse(element.getElement("math")!.innerText);
  List<Param> args = List.from(element.findElements("variable").map((e) =>
      params.firstWhere((element) => element.id == e.getAttribute("param"))));
  return new Formula(
      params.firstWhere((e) => e.id == element.getAttribute("param")),
      expression,
      element.getAttribute("latex")!,
      args);
}
