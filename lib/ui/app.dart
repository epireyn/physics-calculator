import 'package:decimal/decimal.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_math_fork/flutter_math.dart';
import 'package:physics_calculator/ui/widgets/fields.dart';

import '../calculation.dart';
import '../i18n/localization.dart';
import '../i18n/strings.dart';
import '../units.dart';
import '../main.dart';

var CALCULATIONS = [];

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Calculation? choice;
  _BodyPage? _body;
  late GlobalKey<_BodyPageState> _bodyKey;
  late Future<List<Calculation>> calculations;

  @override
  void initState() {
    calculations = loadCalculations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: calculations,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (CALCULATIONS.isEmpty) {
            CALCULATIONS = snapshot.data ?? [];
          }
          CALCULATIONS.sort((a, b) => translatedName(context, a.name)
              .toLowerCase()
              .replaceAll(new RegExp('[éèêë]'), 'e')
              .compareTo(translatedName(context, b.name)
                  .toLowerCase()
                  .replaceAll(new RegExp('[éèêë]'), 'e')));
          if (choice == null) {
            choice = CALCULATIONS.isEmpty ? null : CALCULATIONS[0];
            if (choice != null) {
              _body = _BodyPage(
                choice!,
                choice!.formulas[0],
                key: _bodyKey = new GlobalKey(),
              );
            }
          }
          return Scaffold(
            appBar: AppBar(
              elevation: 0.0,
              title: DropdownButton<Calculation>(
                isExpanded: true,
                selectedItemBuilder: (context) {
                  return CALCULATIONS.map((element) {
                    return Container(
                      constraints: const BoxConstraints(minHeight: 48.0),
                      alignment: AlignmentDirectional.centerStart,
                      child: Text(translatedName(context, element.name)),
                    );
                  }).toList();
                },
                items: CALCULATIONS.map((element) {
                  return DropdownMenuItem<Calculation>(
                    child: Text(
                      translatedName(context, element.name) +
                          (element == choice
                              ? " (${Translator.get(context, Strings.CURRENT).toLowerCase()})"
                              : ""),
                    ),
                    value: element,
                  );
                }).toList(),
                value: choice,
                onChanged: ((i) {
                  setState(() {
                    choice = i;
                  });
                  if (choice != null) {
                    if (_body == null) {
                      _body = _BodyPage(
                        choice!,
                        choice!.formulas[0],
                        key: _bodyKey = new GlobalKey(),
                      );
                    } else {
                      _bodyKey.currentState!
                          .refresh(choice!, choice!.formulas[0]);
                    }
                  }
                }),
              ),
            ),
            body: _body ?? Center(child: Text('Nothing to show')),
          );
        } else if (snapshot.hasError) {
          return Scaffold(
            body: SafeArea(
              child: Center(
                child: Column(
                  children: [
                    Text(
                        Translator.get(context, Strings.INTERNAL_ERROR,
                            params: [snapshot.error]),
                        style: TextStyle(color: Colors.red)),
                    TextButton.icon(
                      onPressed: () {
                        setState(() {
                          calculations = loadCalculations();
                        });
                      },
                      icon: Icon(Icons.refresh),
                      label: Text(Translator.get(context, Strings.RETRY)),
                    )
                  ],
                ),
              ),
            ),
          );
        } else {
          return Scaffold(
            body: SafeArea(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
          );
        }
      },
    );
  }

  String translatedName(context, String name) {
    var split = name.split("+");
    var res = "";
    for (var s in split) {
      res += Translator.getFromString(context, s);
    }
    return res.isEmpty ? name : res;
  }
}

class _BodyPage extends StatefulWidget {
  final Calculation choice;
  final Formula formula;

  _BodyPage(this.choice, this.formula, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BodyPageState();
}

class _BodyPageState extends State<_BodyPage> {
  late Calculation choice;
  late Formula formula;
  late _CalculsPage _calculsPage;
  late GlobalKey<_CalculsPageState> calculsKey;

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    calculsKey = new GlobalKey();
    choice = widget.choice;
    formula = widget.formula;
    _calculsPage = _CalculsPage(
      choice,
      formula,
      _scaffoldKey,
      key: calculsKey,
    );
  }

  List<DropdownMenuItem<Formula>> _createFonctDropdown(context) {
    return choice.params.map((element) {
      Formula from = choice.fromParam(element);
      return DropdownMenuItem(
        child: Text(
          Translator.getFromString(context, element.name) +
              (from == formula
                  ? " (${Translator.get(context, Strings.CURRENT).toLowerCase()})"
                  : ""),
        ),
        value: from,
      );
    }).toList();
  }

  void refresh(choice, fncI) {
    setState(() {
      this.choice = choice;
      this.formula = fncI;
    });
    calculsKey.currentState!.refresh(choice, fncI);
  }

  @override
  Widget build(BuildContext context) {
    var args = choice.params;
    return Scaffold(
      key: _scaffoldKey,
      appBar: args.isEmpty
          ? null
          : AppBar(
              elevation: 1,
              title: DropdownButton<Formula>(
                  dropdownColor: Theme.of(context).primaryColor,
                  isExpanded: true,
                  selectedItemBuilder: (context) {
                    return args.map((element) {
                      return Container(
                        constraints: const BoxConstraints(minHeight: 48.0),
                        alignment: AlignmentDirectional.centerStart,
                        child: Text(
                            Translator.getFromString(context, element.name)),
                      );
                    }).toList();
                  },
                  items: _createFonctDropdown(context),
                  value: formula,
                  onChanged: (i) {
                    setState(() => formula = i!);
                    calculsKey.currentState!.refresh(choice, formula);
                  })),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: ScrollConfiguration(
            behavior: NoGlowingScroll(),
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Center(
                  child: Column(children: <Widget>[
                    Math.tex(
                      formula.latex,
                      textStyle: Theme.of(context)
                          .textTheme
                          .titleLarge
                          ?.copyWith(fontSize: 30),
                    ),
                    Padding(padding: EdgeInsets.symmetric(vertical: 10)),
                    Math.tex(
                      formula.unitsFormula,
                      textStyle: Theme.of(context)
                          .textTheme
                          .titleMedium
                          ?.copyWith(fontSize: 15),
                    ),
                  ]),
                ),
                Container(
                  padding: EdgeInsets.all(16),
                  child: _calculsPage,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _CalculsPage extends StatefulWidget {
  final Calculation choice;
  final Formula formula;
  final GlobalKey<ScaffoldState> parentKey;

  _CalculsPage(this.choice, this.formula, this.parentKey, {Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _CalculsPageState();
}

class _CalculsPageState extends State<_CalculsPage> {
  late Calculation choice;
  late Formula formula;
  GlobalKey<ScaffoldState> pageKey = GlobalKey();
  Unit? responseUnit;

  @override
  void initState() {
    choice = widget.choice;
    formula = widget.formula;
    super.initState();
  }

  void refresh(choice, fncI) {
    setState(() {
      this.choice = choice;
      this.formula = fncI;
      _result.clear();
      responseUnit = null;
    });
  }

  Map<String, dynamic> _result = Map();

  @override
  Widget build(BuildContext context) {
    GlobalKey<FormState> _formKey = GlobalKey();
    List<Field> fields = _getFields();
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Flexible(
              fit: FlexFit.loose,
              child: Column(
                children: fields.map((element) {
                  return Padding(
                    padding: EdgeInsets.only(bottom: 30),
                    child: element,
                  );
                }).toList(),
              )),
          Flexible(
            fit: FlexFit.loose,
            child: MaterialButton(
              color: Theme.of(context).primaryColor,
              child: Text(Translator.get(context, Strings.RESULT)),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  _result.clear();
                  choice
                      .calculate(
                          formula,
                          fields.map((element) {
                            return ValuedParam(
                                formula.params.firstWhere(
                                    (el) => el.name == element.name),
                                element.value.toDouble());
                          }).toList())
                      .then((v) => setState(() {
                            _result['error'] = false;
                            _result['text'] = v;
                          }))
                      .catchError((e) => setState(() {
                            _result['error'] = true;
                            _result['text'] = e;
                          }));
                  fields.clear();
                }
              },
            ),
          ),
          _getResultWidget(),
        ],
      ),
    );
  }

  List<Field> _getFields() {
    return formula.params.map((e) => e.field).toList();
  }

  _getResultWidget() {
    var args = formula.self.units;
    var result = _result['text'];
    var resultString = responseUnit != null
        ? ((result as Decimal).toRational() / responseUnit!.coefficient).toDecimal(scaleOnInfinitePrecision: 3).round(scale: 5).toString()
        : result.toString();
    return Container(
      padding: EdgeInsets.all(10),
      child: result != null
          ? Row(
              textBaseline: TextBaseline.alphabetic,
              crossAxisAlignment: CrossAxisAlignment.baseline,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SelectableText(
                  resultString,
                  style: _result['error']
                      ? TextStyle(color: Colors.red, fontSize: 20)
                      : TextStyle(fontSize: 20),
                  onTap: _result['error'] ? null : (() {
                    Clipboard.setData(ClipboardData(text: result));
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Icon(
                            Icons.done,
                            color: Colors.green,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 5),
                            child: Text(
                                Translator.get(context, Strings.RESULT_COPIED)),
                          ),
                        ],
                      ),
                      duration: Duration(seconds: 2),
                    ));
                  }),
                ),
                args.units.isEmpty
                    ? Container()
                    : args.units.length > 1
                        ? DropdownButton<Unit>(
                          underline: Container(),
                          items: args.units
                              .map((e) => DropdownMenuItem<Unit>(
                                    child: Math.tex(
                                      e.value,
                                      mathStyle: MathStyle.text,
                                    ),
                                    value: e,
                                  ))
                              .toList(),
                          onChanged: ((value) =>
                              setState(() => responseUnit = value)),
                          value: responseUnit ?? args.defaultUnit,
                          selectedItemBuilder: (context) => args.units
                              .map((e) => Align(
                                    child: Math.tex(
                                      e.value,
                                      mathStyle: MathStyle.text,
                                      textStyle: TextStyle(fontSize: 20),
                                    ),
                                  ))
                              .toList(),
                        )
                        : Math.tex(args.units[0].value,
                            mathStyle: MathStyle.text,
                            textStyle: TextStyle(fontSize: 20)),
              ],
            )
          : Container(),
    );
  }
}
