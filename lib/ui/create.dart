import 'package:flutter/material.dart';
import 'package:flutter_math_fork/flutter_math.dart';
import 'package:physics_calculator/calculation.dart';
import 'package:physics_calculator/i18n/localization.dart';
import 'package:physics_calculator/ui/widgets/fields.dart';
import 'package:physics_calculator/units.dart';

import '../i18n/strings.dart';

class CreatePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {
  @override
  Widget build(BuildContext context) {
    TabBar bar = TabBar(
      tabs: [
        Tab(
          icon: Icon(Icons.add),
        ),
        Tab(
          icon: Icon(Icons.edit),
        )
      ],
    );
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: PreferredSize(
            child: Container(
              child: bar,
              color: Theme.of(context).primaryColor,
            ),
            preferredSize: bar.preferredSize,
          ),
          body: TabBarView(children: [
            _EditPage(null),
            _ListPage(),
          ]),
        ));
  }
}

class _EditPage extends StatefulWidget {
  final Calculation? calculation;

  const _EditPage(this.calculation);

  @override
  _EditPageState createState() => _EditPageState();
}

class _EditPageState extends State<_EditPage> {
  late _ParamsEdit _paramsEdit;
  GlobalKey<FormState> _formKey = new GlobalKey();
  List<Param>? newParams;
  String? newName;

  @override
  void initState() {
    _paramsEdit = _ParamsEdit(
      initialValue:
          widget.calculation?.params ?? const [],
      onSaved: (newValue) => newParams = newValue,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Form(
          key: _formKey,
          child: ListBody(
            children: [
              TextFormField(
                initialValue:
                    widget.calculation?.name ?? '',
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return Translator.get(context, Strings.REQUIRED_FIELD);
                  }
                  return null;
                },
                onSaved: (newValue) => newName = newValue,
              ),
              _paramsEdit,
            ],
          ),
        ),
        TextButton(
            onPressed: _save,
            child: Text(Translator.get(context, Strings.CREATE))),
      ],
    );
  }

  _save() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      if (widget.calculation != null) {
        widget.calculation!.name = newName!;
        widget.calculation!.params = newParams!;
      }
    }
  }
}

class _ParamsEdit extends FormField<List<Param>> {
  _ParamsEdit(
      {FormFieldSetter<List<Param>>? onSaved,
      List<Param> initialValue = const [],
      AutovalidateMode autovalidateMode = AutovalidateMode.disabled,
      FormFieldValidator<List<Param>>? validator})
      : super(
          onSaved: onSaved,
          initialValue: initialValue,
          autovalidateMode: autovalidateMode,
          validator: validator,
          builder: (state) {
            return ListView.builder(
              itemCount: state.value!.length,
              itemBuilder: (context, index) {
                if (index + 1 == state.value!.length) {
                  return ListTile(
                    title: Center(child: Icon(Icons.add_circle)),
                  );
                }
                return _getTile(state, state.value![index]);
              },
            );
          },
        );

  static ListTile _getTile(FormFieldState<List<Param>> state, Param param) {
    return ListTile(
      leading: IconButton(
          onPressed: () => _openEditPopUp(state.context, param),
          icon: Icon(
            Icons.edit,
            textDirection: TextDirection.rtl,
          )),
      title: Row(children: [Text(param.name), Math.tex('(${param.latex})')]),
      subtitle: Math.tex(param.units.defaultUnit.value),
      trailing: IconButton(
          onPressed: () => _confirmDeletePopUp(state.context, () {
                state.value!.remove(param);
                state.didChange(state.value);
              }),
          icon: Icon(Icons.delete)),
    );
  }

  static _openEditPopUp(BuildContext context, Param param) {
    showDialog(
      context: context,
      builder: (context) {
        GlobalKey<_EditParamPopUpBodyState> _key = GlobalKey();
        return AlertDialog(
          title: Text(Translator.get(context, Strings.EDIT_PARAM)),
          content: _EditParamPopUpBody(
            param,
            key: _key,
          ),
          actions: [
            TextButton(
                onPressed: () {
                  if (_key.currentState!._formKey.currentState!.validate()) {
                    var state = _key.currentState!;
                    param.id = state.id;
                    param.name = state.name;
                    param.latex = state.latex;
                    param.units = state.units;
                    param.field = state.field;
                    Navigator.pop(context);
                  }
                },
                child: Text(Translator.get(context, Strings.SAVE))),
            TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text(Translator.get(context, Strings.CANCEL))),
          ],
        );
      },
    );
  }
}

class _FormulasEdit extends StatefulWidget {
  final List<Formula> params;

  const _FormulasEdit(this.params, {Key? key}) : super(key: key);

  @override
  _FormulasEditState createState() => _FormulasEditState();
}

class _FormulasEditState extends State<_FormulasEdit> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class _ListPage extends StatefulWidget {
  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<_ListPage> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

_confirmDeletePopUp(BuildContext context, VoidCallback onConfirmed) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(
          Translator.get(context, Strings.CONFIRM_DELETE_TITLE),
          style: TextStyle(color: Colors.red),
        ),
        actions: [
          TextButton(
              onPressed: onConfirmed,
              child: Text(Translator.get(context, Strings.DELETE))),
          TextButton(
              onPressed: onConfirmed,
              child: Text(Translator.get(context, Strings.CANCEL))),
        ],
      );
    },
  );
}

class _EditParamPopUpBody extends StatefulWidget {
  final Param param;
  const _EditParamPopUpBody(this.param, {Key? key}) : super(key: key);

  @override
  _EditParamPopUpBodyState createState() => _EditParamPopUpBodyState();
}

class _EditParamPopUpBodyState extends State<_EditParamPopUpBody> {
  final _formKey = GlobalKey<FormState>();
  late String id;
  late String name;
  late String latex;
  late Units units;
  late Field field;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: ListBody(
        children: [
          TextFormField(
            maxLength: 1,
            keyboardType: TextInputType.text,
            initialValue: widget.param.id,
            decoration:
                InputDecoration(hintText: Translator.get(context, Strings.ID)),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return Translator.get(context, Strings.REQUIRED_FIELD);
              }
              return null;
            },
            onFieldSubmitted: (value) => id = value,
          ),
          TextFormField(
            initialValue: widget.param.name,
            decoration: InputDecoration(
                hintText: Translator.get(context, Strings.NAME)),
            validator: (value) {
              if (value == null || value.isEmpty) {
                return Translator.get(context, Strings.REQUIRED_FIELD);
              }
              return null;
            },
            onFieldSubmitted: (value) => name = value,
          ),
          LaTeXPreviewField(
            initialValue: widget.param.latex,
            onSaved: (newValue) => latex = newValue!,
          ),
          //TODO Units field
          //TODO Field field;
        ],
      ),
    );
  }
}
