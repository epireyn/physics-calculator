import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../i18n/localization.dart';
import '../i18n/strings.dart';
import '../main.dart';

const _REPORT_URL = 'https://gitlab.com/epireyn/physics-calculator/-/issues';

void openSettings(context, int value) async {
  switch (value) {
    case 0:
      showDialog<AlertDialog>(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(
            Translator.get(context, Strings.LANGUAGE),
          ),
          content: _LanguageSelector(),
          actions: [
            TextButton(
                onPressed: () => Navigator.pop(context), child: Text('Ok')),
          ],
        ),
      );
      break;
    case 1:
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(
            Translator.get(context, Strings.THEME),
          ),
          content: _ThemeSelector(),
          actions: [
            TextButton(
                onPressed: () => Navigator.pop(context), child: Text('Ok')),
          ],
          scrollable: true,
        ),
      );
      break;
    case 2:
      if (await canLaunchUrlString(_REPORT_URL)) {
        await launchUrlString(_REPORT_URL);
      } else {
        showDialog<AlertDialog>(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(
              Translator.get(context, Strings.REPORT_BUG),
            ),
            content: SelectableText(
              _REPORT_URL,
              onTap: () => Clipboard.setData(ClipboardData(text: _REPORT_URL)),
            ),
            actions: [
              TextButton(
                  onPressed: () => Navigator.pop(context), child: Text('Ok')),
            ],
          ),
        );
      }
      break;
    case 3:
      PackageInfo.fromPlatform().then((infos) {
        showAboutDialog(
            context: context,
            applicationName: infos.appName,
            applicationIcon: Image.asset(
              'assets/aboutIcon.png',
              height: 40,
              width: 40,
            ),
            applicationVersion: infos.version + '-' + infos.buildNumber,
            applicationLegalese:
                Translator.get(context, Strings.LEGALESE_TEXT));
      });
      break;
  }
}

class _LanguageSelector extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LanguageSelectorState();
}

class _LanguageSelectorState extends State<_LanguageSelector> {
  var lg;
  var _preference;

  @override
  void initState() {
    SharedPreferences.getInstance().then((value) => setState(() {
          lg = value.getString('lg') ?? 'auto';
          _preference = value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var locales = List.of(localizedSimpleValues.keys, growable: true);
    locales.insert(0, 'auto');
    return lg == null
        ? Center(
            child: CircularProgressIndicator(),
          )
        : SizedBox(
            width: 300,
            height: 300,
            child: ListView.builder(
              itemCount: locales.length,
              itemBuilder: (context, index) => RadioListTile(
                  title: Text(locales[index]),
                  value: locales[index],
                  groupValue: lg,
                  onChanged: (value) => _change(context, value)),
            ),
          );
  }

  _change(context, value) async {
    TranslationDelegate newLocale;

    if (value == 'auto') {
      newLocale = TranslationDelegate();
    } else {
      var split = value.split('_');
      newLocale = SpecifiedTranslationDelegate(
          Locale(split[0], split.length > 1 ? split[1] : ''));
    }

    setState(() {
      lg = value;
      changeDelegate.call(newLocale);
    });
    _preference.setString('lg', value);
  }
}

class _ThemeSelector extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ThemeSelectorState();
}

class _ThemeSelectorState extends State<_ThemeSelector> {
  var current = themeProvider.darkTheme;

  @override
  Widget build(BuildContext context) {
    return ListBody(
      children: [
        _tile('auto', 0),
        _tile(Translator.get(context, Strings.LIGHT), 1),
        _tile(Translator.get(context, Strings.DARK), 2)
      ],
    );
  }

  Widget _tile(text, value) {
    return RadioListTile<int>(
      groupValue: current,
      value: value,
      onChanged: (value) {
        themeProvider.darkTheme = value!.toInt();
        setState(() {
          current = value;
        });
      },
      title: Text(text),
    );
  }
}
