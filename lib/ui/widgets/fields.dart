import 'package:decimal/decimal.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_math_fork/flutter_math.dart';
import 'package:rational/rational.dart';

import '../../i18n/localization.dart';
import '../../i18n/strings.dart';
import '../../units.dart';

final formatters = [
  _CalculInputFormatter(),
  FilteringTextInputFormatter.deny(' '),
];

abstract class Field<T extends State<StatefulWidget>> extends StatefulWidget {
  final String name;
  final GlobalKey<T> key;

  Field(this.name, {GlobalKey<T>? key})
      : this.key = key ?? (key = GlobalKey()),
        super(key: key);

  Rational get value;

  void reset();
}

class NumberField extends Field<_NumberFieldState> {
  NumberField(String name, {GlobalKey<_NumberFieldState>? key})
      : super(name, key: key);

  @override
  State<StatefulWidget> createState() => _NumberFieldState();

  @override
  void reset() {
    key.currentState!.controller.clear();
  }

  @override
  Rational get value => Decimal.parse(key.currentState!.controller.text).toRational();

  String? validate() {
    return key.currentState!.formField.validator!
        .call(key.currentState!.controller.text);
  }
}

class _NumberFieldState extends State<NumberField> {
  late FormField<String> formField;
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) => formField = FormField<String>(
        builder: (FormFieldState state) {
          return TextField(
            maxLines: 1,
            decoration:
                InputDecoration(hintText: Translator.getFromString(context, widget.name)),
            keyboardType: TextInputType.number,
            controller: controller,
            textInputAction: TextInputAction.done,
            inputFormatters: formatters,
            onChanged: state.didChange,
          );
        },
        validator: (value) {
          if (value == null || value.isEmpty)
            return Translator.get(context, Strings.REQUIRED_FIELD);
          return null;
        },
      );
}

class UnitsField extends Field<_UnitsFieldState> {
  final Units units;
  final NumberField _textField;

  UnitsField(String name, this.units, {GlobalKey<_UnitsFieldState>? key})
      : _textField = NumberField(name),
        super(name, key: key);

  @override
  State<StatefulWidget> createState() => _UnitsFieldState(units.units.first);

  @override
  void reset() {
    _textField.reset();
    key.currentState!.reset();
  }

  @override
  Rational get value =>
      _textField.value * key.currentState!.selectedUnit.coefficient;

  String? validate() {
    return key.currentState!.formField.validator!.call(null);
  }
}

class _UnitsFieldState extends State<UnitsField> {
  Unit selectedUnit;
  late FormField formField;

  _UnitsFieldState(this.selectedUnit);

  @override
  void initState() {
    selectedUnit = widget.units.units[widget.units.def];
    super.initState();
  }

  @override
  Widget build(BuildContext context) => formField = FormField<String>(
        builder: (FormFieldState state) {
          return Row(
              textBaseline: TextBaseline.alphabetic,
              crossAxisAlignment: CrossAxisAlignment.baseline,
              children: <Widget>[
                Flexible(
                  child: widget._textField,
                ),
                widget.units.showUnit
                    ? widget.units.units.length > 1
                        ? DropdownButton<Unit>(
                            underline: Container(),
                            selectedItemBuilder: (BuildContext context) {
                              return widget.units.units.map<Widget>((element) {
                                return Align(
                                  alignment: Alignment.centerRight,
                                  child: Math.tex(element.value,
                                      mathStyle: MathStyle.text, textStyle: Theme.of(context).textTheme.labelLarge?.copyWith(fontSize: 18),),
                                );
                              }).toList();
                            },
                            items: widget.units.units.map((element) {
                              return DropdownMenuItem<Unit>(
                                child: Math.tex(element.value,
                                    mathStyle: MathStyle.text),
                                value: element,
                              );
                            }).toList(),
                            onChanged: ((value) {
                              setState(() {
                                selectedUnit = value!;
                              });
                            }),
                            value: selectedUnit,
                          )
                        : Container(
                            constraints: BoxConstraints.tightFor(),
                            child: Math.tex(
                              selectedUnit.value,
                              mathStyle: MathStyle.text,
                            ),
                          )
                    : Container(),
              ]);
        },
        validator: (value) => widget._textField.validate(),
      );

  void reset() {
    setState(() {
      selectedUnit = widget.units.units.first;
    });
  }
}

class MultipleChoicesField extends Field<_MultipleChoicesFieldState> {
  static final OTHER_INT = BigInt.from(-100000);
  final List<MultipleChoice<Decimal>> choices;
  final UnitsField _unitField;
  final Units units;

  MultipleChoicesField(String name, this.choices, this.units,
      {GlobalKey<_MultipleChoicesFieldState>? key})
      : _unitField = UnitsField(name, units),
        super(name, key: key);

  @override
  State<StatefulWidget> createState() => _MultipleChoicesFieldState();

  @override
  void reset() {
    key.currentState!.reset();
  }

  @override
  Rational get value => key.currentState?.value!.value.toBigInt() == OTHER_INT
      ? _unitField.value
      : key.currentState!.value!.value.toRational();
}

class _MultipleChoicesFieldState extends State<MultipleChoicesField> {
  MultipleChoice<Decimal>? value;

  List<DropdownMenuItem<MultipleChoice<Decimal>>> _createDropdown() =>
      widget.choices.map((element) {
        return DropdownMenuItem(
          child: buildElement(element),
          value: element,
        );
      }).toList();

  Widget buildElement(MultipleChoice<Decimal> element) => Row(
    children: [
      Text(Translator.getFromString(context, element.text)),
      Padding(padding: EdgeInsets.only(right: 5)),
      element.value.toBigInt() == MultipleChoicesField.OTHER_INT
          ? Container()
          : Math.tex(
        '(${element.value}\\ ${widget.units.units[widget.units.def].value})', mathStyle: MathStyle.text, textStyle: Theme.of(context).textTheme.labelLarge?.copyWith(fontSize: 15),)
    ],
  );

  @override
  void initState() {
    if (!widget.choices.any(
        (element) => element.value.toBigInt() == MultipleChoicesField.OTHER_INT))
      widget.choices.add(MultipleChoice<Decimal>(
          Strings.OTHER.name, Decimal.fromBigInt(MultipleChoicesField.OTHER_INT)));
    value = widget.choices.first;
    super.initState();
  }

  @override
  Widget build(BuildContext context) => FormField<MultipleChoice<Decimal>>(
        builder: (FormFieldState state) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              DropdownButton(
                dropdownColor: Theme.of(context).primaryColor,
                selectedItemBuilder: (BuildContext context) {
                  return widget.choices.map<Widget>(buildElement).toList();
                },
                isExpanded: true,
                underline: Container(
                  decoration: BoxDecoration(
                    border: Border(top: BorderSide()),
                  ),
                ),
                items: _createDropdown(),
                onChanged: ((value) {
                  if (this.value!.value.toBigInt() ==
                      MultipleChoicesField.OTHER_INT) {
                    widget._unitField.reset();
                  }
                  state.didChange(this.value = value);
                }),
                value: value,
              ),
              value!.value.toBigInt() == MultipleChoicesField.OTHER_INT
                  ? Container(
                      padding: EdgeInsets.symmetric(vertical: 30),
                      child: widget._unitField,
                    )
                  : Container(),
            ],
          );
        },
        initialValue: value,
        validator: (value) {
          if (value != null &&
              value.value.toBigInt() == MultipleChoicesField.OTHER_INT) {
            return widget._unitField.validate();
          }
          return null;
        },
      );

  void reset() {
    value = null;
  }
}

class MultipleChoice<T> {
  final String text;
  final T value;

  MultipleChoice(this.text, this.value);
}

class _CalculInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    var newStrings = newValue.text.replaceAll(',', '.').split('.');
    if (newStrings.length > 2) return oldValue;
    return newValue;
  }
}

class LaTeXPreviewField extends FormField<String> {
  LaTeXPreviewField(
      {FormFieldSetter<String>? onSaved,
      String initialValue = '',
      AutovalidateMode autovalidateMode = AutovalidateMode.disabled})
      : super(
          onSaved: onSaved,
          initialValue: initialValue,
          autovalidateMode: autovalidateMode,
          validator: (value) => null,
          builder: (state) {
            TextEditingController _controller =
                new TextEditingController(text: initialValue);
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Math.tex(state.value ?? ''),
                TextField(
                  onEditingComplete: () => state.didChange(_controller.text),
                  controller: _controller,
                )
              ],
            );
          },
        );
}
