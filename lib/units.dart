import 'dart:math';

import 'package:decimal/decimal.dart';
import 'package:rational/rational.dart';

final units = {
  "length": LengthUnits(),
  "mass": MassUnits(),
  "velocity": VelocityUnits(),
  "time": TimeUnits(),
  "pressure": PressureUnits(),
  "angle": AngleUnit(),
  "volume": VolumeUnit(),
  "density": densityUnits,
  "gravity": gravityUnits,
  "area": areaUnits,
  "stiffness": stiffness,
};

final Units densityUnits = Units(
    MassUnits()
        .units
        .map((e) => Unit('\\frac{${e.value}}{m^3}', e.coefficient))
        .toList(),
    MassUnits().units.indexWhere((element) => element.value == 'kg'));
final Units gravityUnits = Units(
    MassUnits()
        .units
        .map((e) => Unit('\\frac{N}{${e.value}}', Rational.one / e.coefficient))
        .toList(),
    MassUnits().units.indexWhere((element) => element.value == 'kg'));
final Units areaUnits = Units(
    LengthUnits()
        .units
        .map((e) => Unit('${e.value}^2', e.coefficient * e.coefficient))
        .toList(),
    LengthUnits().def);

final Units stiffness = Units(
    LengthUnits()
        .units
        .map((e) => Unit('\\frac{N}{${e.value}}', e.coefficient))
        .toList(),
    LengthUnits().def);

class Unit {
  final String value;
  final Rational coefficient;

  const Unit(this.value, this.coefficient);

  Unit.fromDecimal(String value, Decimal coefficient)
      : this(value, coefficient.toRational());

  @override
  bool operator ==(Object other) =>
      other is Unit && other.value == value && other.coefficient == coefficient;

  @override
  int get hashCode {
    int hash = 23;
    hash = hash * 31 + value.hashCode;
    hash = hash * 31 + coefficient.hashCode;
    return hash;
  }
}

class Units {
  final List<Unit> units;
  final int def;
  final bool showUnit;

  const Units(this.units, [this.def = 0, this.showUnit = true]);

  Unit get defaultUnit => units[def];
}

class LengthUnits extends Units {
  LengthUnits()
      : super([
          Unit.fromDecimal('mm', Decimal.parse('0.001')),
          Unit.fromDecimal('cm', Decimal.parse('0.01')),
          Unit.fromDecimal('dm', Decimal.parse('0.1')),
          Unit.fromDecimal('m', Decimal.one),
          Unit.fromDecimal('dam', Decimal.fromInt(10)),
          Unit.fromDecimal('hm', Decimal.fromInt(100)),
          Unit.fromDecimal('km', Decimal.fromInt(1000)),
        ], 3);
}

class VelocityUnits extends Units {
  VelocityUnits()
      : super([
          Unit.fromDecimal("\\frac{m}{s}", Decimal.one),
          Unit.fromDecimal("\\frac{m}{m}", Decimal.parse('0.01666666666666')),
          Unit.fromDecimal("\\frac{m}{h}", Decimal.parse('0.00027777777777')),
          Unit.fromDecimal("\\frac{km}{s}", Decimal.fromInt(1000)),
          Unit.fromDecimal("\\frac{km}{m}", Decimal.parse('16.6666666666666')),
          Unit.fromDecimal("\\frac{km}{h}", Decimal.parse('0.27777777777777'))
        ]);
}

class TimeUnits extends Units {
  TimeUnits()
      : super([
          Unit.fromDecimal('ms', Decimal.parse('0.001')),
          Unit.fromDecimal('cs', Decimal.parse('0.01')),
          Unit.fromDecimal('ds', Decimal.parse('0.1')),
          Unit.fromDecimal('s', Decimal.one),
          Unit.fromDecimal('m', Decimal.fromInt(60)),
          Unit.fromDecimal('h', Decimal.fromInt(3600)),
        ], 3);
}

class PressureUnits extends Units {
  PressureUnits()
      : super([
          Unit.fromDecimal("Pa", Decimal.one),
          Unit.fromDecimal("bar", Decimal.fromInt(100000)),
          Unit.fromDecimal("at", Decimal.parse("98066.5")),
          Unit.fromDecimal("atm", Decimal.fromInt(101325)),
          Unit.fromDecimal("Torr", Decimal.parse("133.322368421")),
        ]);
}

class MassUnits extends Units {
  MassUnits()
      : super([
          Unit.fromDecimal("mg", Decimal.parse("0.000001")),
          Unit.fromDecimal("cg", Decimal.parse("0.00001")),
          Unit.fromDecimal("dg", Decimal.parse("0.0001")),
          Unit.fromDecimal("g", Decimal.parse("0.001")),
          Unit.fromDecimal("dag", Decimal.parse("0.01")),
          Unit.fromDecimal("hg", Decimal.parse("0.1")),
          Unit.fromDecimal("kg", Decimal.one),
        ], 6);
}

class AngleUnit extends Units {
  AngleUnit()
      : super([
          Unit.fromDecimal('\\circ', Decimal.parse((pi / 180).toString())),
          Unit.fromDecimal('rad', Decimal.one)
        ], 1, true);
}

class VolumeUnit extends Units {
  VolumeUnit()
      : super([
          Unit.fromDecimal('kl', Decimal.one),
          Unit.fromDecimal('hl', Decimal.parse("10")),
          Unit.fromDecimal('dal', Decimal.parse("100")),
          Unit.fromDecimal('l', Decimal.parse("1000")),
          Unit.fromDecimal('dl', Decimal.parse("10000")),
          Unit.fromDecimal('cl', Decimal.parse("100000")),
          Unit.fromDecimal('ml', Decimal.parse("1000000")),
          Unit.fromDecimal('m^3', Decimal.one),
          Unit.fromDecimal('dm^3', Decimal.parse("1000"))
        ], 7);
}
